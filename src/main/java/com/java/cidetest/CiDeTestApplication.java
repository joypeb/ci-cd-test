package com.java.cidetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiDeTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiDeTestApplication.class, args);
    }

}
